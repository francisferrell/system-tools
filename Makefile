
# debian tools will run `make` at the root, but we don't need anything done
# so we need a no-op target
make-for-debian:


.PHONY: release
release:
	@if [ -n "$(VERSION)" ]; then true; else echo "Error: VERSION not specified. If you're calling 'make release' directly, make sure you know what you're doing!"; exit 2; fi
	@if [ "$$( git rev-parse --abbrev-ref HEAD )" = 'main' ]; then true; else echo "Error: branch is '$$( git rev-parse --abbrev-ref HEAD )'; expected 'main'"; exit 2; fi
	@echo Releasing $(VERSION)!
	@echo
	git fetch origin
	git tag '$(VERSION)'
	git checkout ubuntu
	git reset --hard origin/ubuntu
	git merge main --no-edit --message 'merge main into ubuntu for release of $(VERSION)'
	gbp dch --new-version='$(VERSION)-1' --release --distribution=eoan --force-distribution --commit --commit-msg='release $(VERSION) for ubuntu' --spawn-editor=never
	git tag 'ubuntu/$(VERSION)-1'
	git checkout main
	@echo
	@echo Release workflow complete. Now run:
	@echo
	@echo '       git push origin ubuntu'
	@echo '       git push --tags'
	@echo



.PHONY: revbump
revbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1+1}' )"

