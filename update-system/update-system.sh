#!/bin/bash


interactive() {
    [[ -t 1 ]]
}


indent_reboot_pkgs() {
    sed -e 's/^/    /' /run/reboot-required.pkgs
}


update_system() {
    if ! interactive ; then
        export DEBIAN_FRONTEND="noninteractive"
    fi

    apt-get update
    apt-get -y dist-upgrade
    apt-get -y autoremove

    if [[ -e /run/reboot-required ]] ; then
        if interactive ; then
            if [[ -e /run/reboot-required.pkgs ]] ; then
                echo
                echo "    a reboot is required because:"
                indent_reboot_pkgs
            else
                echo
                echo "    a reboot is required"
            fi
        else
            if [[ -e /run/reboot-required.pkgs ]] ; then
                echo "rebooting because:"
                indent_reboot_pkgs
            else
                echo "rebooting"
            fi

            reboot
        fi
    else
        if interactive ; then
            echo
            echo "    no reboot is required"
        else
            echo "no reboot is required"
        fi
    fi
}


if [[ ${BASH_SOURCE[0]} == $0 ]] ; then
    update_system
fi

