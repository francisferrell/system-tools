#! /bin/bash

WE_MOUNTED=false



log() {
    echo "$@" 1>&2
}


abort() {
    log "$@"
    exit 1
}


cleanup() {
    if [[ $WE_MOUNTED == true ]] ; then
        log "unmounting firebackup"
        umount /mnt/firebackup
        WE_MOUNTED=false
    fi
}



main() {
    set -e
    trap cleanup INT TERM EXIT

    if ! mountpoint -q /mnt/firebackup ; then
        log "mounting firebackup"
        mkdir -p /mnt/firebackup
        mount /dev/disk/by-label/firebackup /mnt/firebackup || abort 'failed to mount drive'
        WE_MOUNTED=true
    fi

    local hostname="$( hostname )"

    if [[ -z $hostname ]] ; then
        abort "hostname is empty"
    fi

    local destination="/mnt/firebackup/${hostname}"
    mkdir -p "$destination"

    rsync \
        --verbose \
        --archive \
        --prune-empty-dirs \
        --include "*/" \
        --filter='merge /firebackup.filter' \
        "$@" \
        / \
        "$destination"

    cleanup
}

main "$@" 2>&1 | tee >( systemd-cat -t firebackup )

