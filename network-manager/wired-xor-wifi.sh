#!/bin/bash

tag='wired-xor-wifi'
interface="$1"
action="$2"

if [[ -z $interface ]] ; then
    exit
elif [[ $action != 'up' && $action != 'down' ]] ; then
    exit
fi

interface_type="$( nmcli device show "$interface" | awk '$1 == "GENERAL.TYPE:" { print $2 }' )"
# if the device disappears, e.g. when disconnecting from my thunderbolt 3 dock,
# then `nmcli device show foo` produces `Error: Device 'foo' not found.` on
# stderr and no stdout. So, if the result from awk is empty, assume it was an
# ethernet device that disappeared.
#
# if a WIFI device disappears, e.g. a USB WIFI adapter is unplugged, then
# the result could be unexpected behavior. Fortunately, this isn't *my* use case.
if [[ -z $interface_type ]] ; then
    interface_type='ethernet'
fi

if [[ $interface_type != 'ethernet' ]] ; then
    logger -t "$tag" -p user.info "$interface is $action"
    exit
fi

if [[ $action = 'up' ]] ; then
    logger -t "$tag" -p user.notice "$interface is up, disabling WIFI"
    nmcli radio wifi off
elif [[ $action = 'down' ]] ; then
    logger -t "$tag" -p user.notice "$interface is down, enabling WIFI"
    nmcli radio wifi on
fi

