#!/bin/bash

function main() {
    set -euo pipefail
    source ~/.config/s3-backup.conf
    [[ -n $S3_BUCKET ]]

    local local_path="$( mktemp --directory --tmpdir=. s3-backup.XXXXXX )"
    local s3_path="${S3_BUCKET}/${S3_PREFIX}${1:-/}"

    aws s3 sync --sse-c --sse-c-key "$ENCRYPTION_KEY" "$s3_path" "$local_path" "$@"

    echo
    echo "    S3 backup is available in ${local_path}/"
}



if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main "$@"
fi

