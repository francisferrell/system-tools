#!/bin/bash

function msg() {
    echo '[s3-backup]' "$@" 1>&2
}



function backup() {
    local local_path="$1"
    shift
    local s3_path="${S3_BUCKET}/${S3_PREFIX}${local_path}"

    msg "backing up '$local_path' to '$s3_path'"
    if [[ -d $local_path ]] ; then
        aws s3 sync "$local_path" "$s3_path" "$@"
    elif [[ -f $local_path ]] ; then
        local local_base_dir="$( dirname "$local_path" )"
        local s3_base_dir="${S3_BUCKET}/${S3_PREFIX}${local_base_dir}"
        aws s3 sync "$local_base_dir" "$s3_base_dir" "$@" --exclude '*' --include "$( basename "$local_path" )"
    fi
}

function backup_encrypted() {
    backup "$@" --sse-c --sse-c-key "$ENCRYPTION_KEY"
}



function main() {
    set -euo pipefail
    source /etc/s3-backup.conf
    [[ -n $S3_BUCKET ]]

    if [[ -e /etc/s3-backup-encrypted.manifest ]] ; then
        [[ -n $ENCRYPTION_KEY ]]

        while read -r line ; do
            if [[ -z $line ]] ; then
                true
            elif [[ ! -e $line ]] ; then
                msg "skipping non-existant $line"
            else
                backup_encrypted "$line" "$@"
            fi
        done </etc/s3-backup-encrypted.manifest
    fi

    if [[ -e /etc/s3-backup-unencrypted.manifest ]] ; then
        while read -r line ; do
            if [[ -z $line ]] ; then
                true
            elif [[ ! -e $line ]] ; then
                msg "skipping non-existant $line"
            else
                backup "$line" "$@"
            fi
        done </etc/s3-backup-unencrypted.manifest
    fi
}



if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main "$@"
fi

